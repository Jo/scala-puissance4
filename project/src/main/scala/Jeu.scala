package jeu
import scala.collection.mutable

 class Jeu(i: Int, j: Int){

    type Matrix = Array[Array[Int]]

    val m : Int = i //Nombre de lignes
    val n : Int = j //Nombre de colonnes
    var grille : Matrix = Array.ofDim[Int](m, n).map( {x => x.map( {x => 0})}); //Initialisation de la grille avec des 0 (pas de disque)

    // -------------------------------------------------------------------------//
    // Constructeur auxiliaire pour initialiser le jeu à partir d'une grille déjà avancée
    // -------------------------------------------------------------------------//
    def this( plateau: Array[Array[Int]], i: Int, j: Int){
        this(i, j)
        this.grille = plateau
    }
    

    // -------------------------------------------------------------------------//
    // Affiche la matrice telle qu'elle est stockée (0 => vide; 1 => joueur 1; 2 => joueur2)
    // -------------------------------------------------------------------------//
    def afficherMatrice() : Unit = {
        grille.map( x=> println(x.toList))
    }

    // -------------------------------------------------------------------------//
    // Fonction qui permet d'afficher la matrice correspondant au plateau de jeu
    // -------------------------------------------------------------------------//
    def afficherJeu() : Unit = {
        val s = ""
        val indices = 1 to n
        print("   ")
        indices.foreach(x => print(x + "  "))
        println
        grille.foreach( row => { print(grille.indexOf(row)+1 + "  "); row.foreach( x => {
            x match{
                case 0 => print(".  ")  //Pas de jeton
                case 1 => print("X  ")  //Jeton joueur 1
                case 2 => print("O  ")  //Jeton joueur 2
            } 
        }); println; println} );
    }

    // -------------------------------------------------------------------------//
    // Retourne la ligne numéro i. Attention i doit être compris entre 1 et n 
    // -------------------------------------------------------------------------//
    def getLigne(i: Int) : List[Int] ={
        grille(i-1).toList
    }

    // -------------------------------------------------------------------------//
    // Retourne la colonne numéro i. Attention i doit être compris entre 1 et n 
    // -------------------------------------------------------------------------//
    def getColonne(j: Int): List[Int] ={
        grille.map({row => row(j-1)} ).toList
        
    }

    
    // -------------------------------------------------------------------------//
    // Retourne la diagonale haute-gauche vers bas-droite (i et j augmentent tous les deux)
    // -------------------------------------------------------------------------//
    def getDiagonale1(x: Int, y: Int): List[Int] = {
        var diag = new mutable.ListBuffer[Int]()
        var i = x-1
        var j = y-1
        while(i > 0 && j>0){
            i -= 1
            j -= 1
        }
        while(i < m  && j < n){
            
            diag += grille(i)(j)
            i += 1
            j += 1
        }
        diag.toList
    }

    
    // -------------------------------------------------------------------------//
    // Retourne la diagonale haut-droite vers bas-gauche (i diminue et j augmente)
    // -------------------------------------------------------------------------//
    def getDiagonale2(x: Int, y: Int): List[Int] = {
        var diag = new mutable.ListBuffer[Int]()
        var i = x-1
        var j = y-1
        while(i > 0 && j+1<n){
            i -= 1
            j += 1
        }
        while(i < m  && j >= 0){
            
            diag += grille(i)(j)
            i += 1
            j -= 1
        }
        diag.toList
    }


    // -------------------------------------------------------------------------//
    // Fonction qui test s'il y a 4 fois le numéro du joueur dans une liste
    // -------------------------------------------------------------------------//
    def testVictoireList(l: List[Int], numJoueur: Int): Boolean = {
        val pattern = List.fill(4)(numJoueur)
        l.tails exists (_.startsWith(pattern)) // On vérifie s'il n'y a pas 4 jetons consécutifs dans la liste
    }

    // -------------------------------------------------------------------------//
    // Fonction qui permet de déterminer si le joueur a fait une ligne ou colonne ou diagonale
    // juste après avoir joué dans la case (i, j)
    // -------------------------------------------------------------------------//
    def testVictoire(i: Int, j: Int, numJoueur: Int) : Boolean ={
        
        val colonne     = testVictoireList(getColonne(j), numJoueur)
        val ligne       = testVictoireList(getLigne(i), numJoueur)
        val diag1       = testVictoireList(getDiagonale1(i, j), numJoueur)
        val diag2       = testVictoireList(getDiagonale2(i, j), numJoueur)

        colonne || ligne || diag1 || diag2 

    }

    // -------------------------------------------------------------------------//
    // Retourne vrai si le joueur donné a gagné, à partir de l'état du jeu.
    // Vérifie toutes les lignes, colonnes et diagonale
    // -------------------------------------------------------------------------//
    def testVictoire2(joueur: Int): Boolean ={
        val lignes          = grille.map(x => testVictoireList(x.toList, joueur))
        val vlignes         = lignes.reduce((x, y) => x || y)

        val colonnes        = (1 to n).map(j => testVictoireList(getColonne(j).toList, joueur))
        val vcolonnes       = colonnes.reduce((x, y) => x || y)

        val diag1_inf       = (1 to m).map(i => testVictoireList(getDiagonale1(i, 1), joueur))
        val diag1_sup       = (2 to n).map(j => testVictoireList(getDiagonale1(1, j), joueur))
        val vdiagonales1    = {diag1_inf.reduce((x, y) => x || y)      ||      diag1_sup.reduce((x, y) => x || y)}

        val diag2_inf       = (1 to m).map(i => testVictoireList(getDiagonale2(i, 1), joueur))
        val diag2_sup       = (2 to n).map(j => testVictoireList(getDiagonale2(m, j), joueur))
        val vdiagonales2    = {diag2_inf.reduce((x, y) => x || y)      ||      diag2_sup.reduce((x, y) => x || y)}

        vlignes || vcolonnes || vdiagonales1 || vdiagonales2
    }

    // -------------------------------------------------------------------------//
    // Retourne vrai si la colonne donnée est valide (non saturée ==> contient au moins un 0)
    // -------------------------------------------------------------------------//
    def coupValide(numCol: Int) : Boolean = {
        val v = getColonne(numCol) 
        v.contains(0)
    }


    
    // -------------------------------------------------------------------------//
    // Fonction qui fait jouer un joueur en colonne numCol
    // 1 <= numCol <= n
    // Retourne Vrai si ce coup fait gagner la partie, faux sinon
    // -------------------------------------------------------------------------//
    def joue(numCol: Int, numJoueur: Int) : Boolean = {
        val v = getColonne(numCol)                         //Colonne numCol
        val i = v.lastIndexOf(0)                           //Indice de la ligne où le jeton va "tomber"
        grille(i)(numCol-1) = numJoueur
        testVictoire(i+1, numCol, numJoueur)
        
    }

}
