package ia

import jeu.Jeu
import java.{util => ju}
import scala.util.Random
import scala.math

case class IA (plateau: Jeu, num: Int, numOpp: Int)
{
    var jeu : Jeu = plateau //L'IA est définie à partir d'un plateau de jeu
    var grille = jeu.grille
    val IA      = num       // Numéro joueur de l'IA
    val joueur  = numOpp    // Numéro du joueur réel
    val m = jeu.m           // Nombre de lignes dans le jeu
    val n = jeu.n           // Nombre de colonnes

    
    // -------------------------------------------------------------------------//
    // Retourne la liste des numéros des colonnes où l'utilisateur peut jouer
    // -------------------------------------------------------------------------//
    def coupsValides(jeu: Jeu) : List[Int] = {
        val cols : List[Int] =  (1 to n).toList
        cols.filter( {x => jeu.coupValide(x)}).toList
    }


    
    // -------------------------------------------------------------------------//
    // Retourne le score du joueur, selon l'état du jeu donné en paramètre
    // -------------------------------------------------------------------------//
    def score_position(game: Jeu, numJoueur: Int, initialScore: Int) : Int = {
        //-- Score colonne centrale -- //
        val scoreColCentre = 5 * game.getColonne(n/2+1).count(x => x == numJoueur)

        //-- Score horizontal --//
        var scoresParLignes = game.grille.map( {line => getScoreList(line.toList, numJoueur, initialScore)})
        val scoreLignes     = scoresParLignes.reduce((x,y) => x+y)

        //-- Score vertical --//
        val scoresParColonnes = (1 to n).map(i => getScoreList(game.getColonne(i).toList, numJoueur, initialScore))
        val scoreColonnes       = scoresParColonnes.reduce((x, y) => x+y)

        //--- Calcul du score des diagonales haut-gauche vers bas-droit --//
        val scoreParDiag1_inf   = (1 to m).map(i => getScoreList(game.getDiagonale1(i, 1).toList, numJoueur, initialScore))
        val scoreParDiag1_sup   = (2 to n).map(j => getScoreList(game.getDiagonale1(1, j).toList, numJoueur, initialScore))
        val scorediag1          = scoreParDiag1_inf.reduce((x, y) => x+y) + scoreParDiag1_sup.reduce((x, y) => x+y)

        //--- Calcul du score des diagonales haut-droit vers bas-gauche --//
        val scoreParDiag2_inf   = (1 to m).map(i => getScoreList(game.getDiagonale2(i, 1).toList, numJoueur, initialScore))
        val scoreParDiag2_sup   = (2 to n).map(j => getScoreList(game.getDiagonale2(m, j).toList, numJoueur, initialScore))
        val scorediag2          = scoreParDiag2_inf.reduce((x, y) => x+y) + scoreParDiag1_sup.reduce((x, y) => x+y)

        //-- Score total de l'état du jeu --//
        val score = scoreLignes + scoreColonnes + scorediag1 + scorediag2 + scoreColCentre
        score
    }

    
    // -------------------------------------------------------------------------//
    // Retourne le score d'une liste
    // -------------------------------------------------------------------------//
    def getScoreList(list: List[Int], numJoueur: Int, s: Int): Int = {
        var adversaire = 1
        if (numJoueur == 1) adversaire = 2
        list match{
            case Nil    => s
            //-- Situations critiques ou favorables dont l'IA calcule le score (pénalités ou bonus) --//                 //Valeurs à modifier 
            case a::b::c::d::res        if (a == b && b == c && c == d && a == numJoueur)   => getScoreList(res, numJoueur, s+100)
            case a::b::c::res           if (a == b && b == c && a == numJoueur)             => getScoreList(res, numJoueur, s+10) 
            case 0::a::b::c::res        if (a == b && b == c && a == adversaire)            => getScoreList(res, numJoueur, s-100)
            case a::b::c::0::res        if (a == b && b == c && a == adversaire)            => getScoreList(res, numJoueur, s-100)
            case a::b::0::c::res        if (a == b && b == c && a == adversaire)            => getScoreList(res, numJoueur, s-80)
            case 0::a::b::0::res        if (a == b && a == adversaire)                      => getScoreList(res, numJoueur, s-80)
            case a::0::b::c::res        if (a == b && b == c && a == adversaire)            => getScoreList(res, numJoueur, s-80)
            case a::b::res              if (a==b && b == numJoueur)                         => getScoreList(res, numJoueur, s+5)
            case _::res => getScoreList(res, numJoueur, s)
        }
    }

    
    // -------------------------------------------------------------------------//
    // Retourne le numéro de la colonne à jouer, dont le score sera maximum pour le joueur
    // -------------------------------------------------------------------------//
    def choisirMeilleurCoup(game: Jeu, numJoueur: Int) : Int = {
        //-- Parmi les coups valides --//
        val valides = coupsValides(game)
        //-- On calcule les scores de tous les coups valides possibles, et on choisit le score maximal --//
        val scores: List[(Int, Int)] = valides.map({ numCol => 
            val tempGrille = new Jeu(m, n)                              //On simule un nouveau jeu à partir du jeu actuel 
            tempGrille.grille = game.grille.map(_.clone)                
            tempGrille.joue(numCol, numJoueur)                          //Simulation d'un tour    
            val score = score_position(tempGrille, numJoueur, 0)
            (numCol, score)                                             //On regarde le score pour chaque coup possible    
        }).sortBy(x => x._2)                                            
        val indexOfMAxscore = scores.indexOf(scores(scores.length -1))  //Cherche le meilleur score
        val maxScore   = scores(indexOfMAxscore)
        if (maxScore._2 > 0) maxScore._1 else 1+Random.nextInt(n)       //Retourne la colonne correspondante

    }


    // -------------------------------------------------------------------------//
    // -------------- TENTATIVE D'IMPLEMENTATION DE MINMAX (dommage..)----------//
    // -------------------------------------------------------------------------//

    def isTerminalNode(game: Jeu): Boolean = {
        //Victoire de l'IA ou du joueur
        game.testVictoire2(1) || game.testVictoire2(2) || coupsValides(game) == 0
    }

    // def minimax(game: Jeu, depth: Int, maximizingPlayer: Boolean):(Int, Int) = {
    //     val valides     = coupsValides(game)
    //     val isTerminal  = isTerminalNode(game)
    //     val res = 0
        
    //     if (depth == 0 || isTerminal) {
    //         //Cas qui met fin au jeu
    //         if (isTerminal){ 
    //             //Victoire joueur IA
    //             if(game.testVictoire2(2)) {
    //                 val res : Int = Int.MaxValue
    //                 (None, res)
    //             }
    //             //Victoire joueur 1
    //             else {  if(game.testVictoire2(1)) {
    //                 val res : Int = Int.MinValue
    //                 (None, res)
    //             }
    //             //Egalité
    //             else (None, 0)}
    //         }

    //         //depth == 0 
    //         else {
    //             (None, score_position(game, 2, 0))
    //         }
    //     }

    //     if (maximizingPlayer){
    //         var value = Int.MinValue
            
    //         var colonne = if (valides.length > 0) valides(Random.nextInt(valides.length)) else 1+Random.nextInt(n+1)
    //         var new_score = 0
    //         //Pour chaque colonne valide
    //         for (col <- valides) {
    //             var tempGame = new Jeu(m, n)            
    //             var tempGrille = game.grille.map(_.clone)                   //On simule un tour dans cette colonne
    //             tempGame.grille = tempGrille
    //             var joue =  tempGame.joue(col, 2)
    //             new_score = minimax(tempGame, depth-1, false)._1       //On calcule le score du nouveau jeu obtenu
    //             if (new_score > value) {                                    
    //                 value = new_score;
    //                 colonne = col;
    //             } 
    //         }
    //         (colonne, new_score)
                
            
    //     }
    //     else {
    //         var value = Int.MaxValue
    //         var colonne = if (valides.length > 0) valides(Random.nextInt(valides.length)) else 1+Random.nextInt(n+1)
    //         var new_score = 0
    //         for (col <- valides) {
    //             var tempGame = new Jeu(m, n)
    //             var tempGrille = game.grille.map(_.clone)
    //             tempGame.grille = tempGrille
    //             tempGame.joue(col, 1)
    //             new_score = minimax(tempGame, depth-1, true)._1
    //             if (new_score < value) {
    //                 value = new_score;
    //                 colonne = col;
    //             }
    //         }
    //         (colonne, value)
    //     }
    // }
}

