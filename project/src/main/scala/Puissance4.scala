/*
Auteur: Joseph BEXIGA
Date  : Dimanche 12 avril 2020 (Pâques)
Description: 
Implémentaiton du jeu Puissance 4 avec une IA en Scala. Voir la documentation pour plus de détail

Version scala: 	2.11.12
Version sbt	:	1.3.8
Compilation	: 	compile
Exécution	:	run
Compiler et exécuter:  ~run

*/

package main

import scala.io.StdIn.readLine
import jeu.Jeu
import ia.IA
import scala.util.Random
import scala.util.Try



object Puissance4 extends App{
    
    type Matrix = Array[Array[Int]];
    
    val m       = 6 //NOmbre de lignes 
    val n       = 7 //Nombre de colonnes
    val IA      = 2 //Numéro joueur IA

    // -------------------------------------------------------------------------//
    // Importe un plateau de jeu à partir d'une chaîne de caractères
    // Attention, le début de la chaîne correspond au bas du plateau
    // Exemple de chaîne: "---RJ--\n---R---"
    // -------------------------------------------------------------------------//
    def importPlateau(chaine: String) : Matrix = {
        val jeu = new Jeu(m, n)

        chaine.split("\n").foreach(
            x => x.zipWithIndex.foreach(c =>
            c._1 match {
                case '-' => 
                case 'R' => jeu.joue(c._2+1, 1)
                case 'J' => jeu.joue(c._2+1, 2)
            }
        ))
        jeu.afficherJeu()
        jeu.grille
        
        }
    
    // -------------------------------------------------------------------------//
    // Retourne la chaîne correspondant au plateau de jeu en cours
    // -------------------------------------------------------------------------//
    def exportPlateau(plateau: Matrix): String = {
        var res = ""
        plateau.foreach(
            line => {line.foreach(
                value => {value match{
                    case 0 => res += "-"
                    case 1 => res += "R"
                    case 2 => res += "J"
                }
            })
            res = res + "\n"
            })
        res
    }

    // -------------------------------------------------------------------------//
    // Caste une chaîne de caractère en Int et en None si impossible
    // -------------------------------------------------------------------------//
    def tryToInt(s: String) = Try(s.toInt).toOption


    // -------------------------------------------------------------------------//
    // -- ---------------------Partie à deux joueurs réels --------------------//
    // -------------------------------------------------------------------------//
    def partie() : Unit = {
        println("Nouvelle partie\n")
        val jeu = new Jeu(m, n)
        var fin : Boolean = false //Booléen qui décrit qu'un joueur a gagné, ou que les 2 joueurs n'ont plus de jetons
        var numJoueur = 0

        // -------------------------------------------------------------------------//
        // Boucle principale du jeu, vérifie si un joueur a gagné à chaque tour
        // -------------------------------------------------------------------------//
        while(!fin){
            numJoueur = 1 + numJoueur%2 //Alterne entre joueur 1 et joueur 2
            jeu.afficherJeu()
            var validInput = false
            // -------------------------------------------------------------------------//
            // Boucle de vérification de l'input
            // -------------------------------------------------------------------------//
            while (!validInput)
            {
                println("Saisir numéro de colonne:  ")
                var input = readLine()
                println("\n")
                tryToInt(input) match
                {
                    
                    case Some(-1)     => System.exit(0) //Permet à l'utilisateur de quitter la partie
                    case None => {                      //Cas d'erreur lorsque l'utilisateur saisit une chaîne de caractères par exemple
                        jeu.afficherJeu()
                        println("/!\\ Erreur, veuillez saisir une valeur comprise entre 1 et " + n + "\nPour quitter, tapez -1\n")
                    }

                    case Some(p) if (p.toInt < 1 || p.toInt > n) => {  //Cas d'erreur lorsque l'utilisateur saisit une chaîne de caractères par exemple
                        jeu.afficherJeu()
                        println("/!\\ Erreur, veuillez saisir une valeur comprise entre 1 et " + n + "\nPour quitter, tapez -1\n")
                    }

                    case Some(p) if (p.toInt >= 1 && p.toInt <= n)  => // Cas normal, l'utilisateur saisit un entier compris entre 1 et n
                        {
                        val numCol = p.toInt
                        if (jeu.coupValide(numCol) )                   // On vérifie que la colonne choisie est valide       
                        {
                            println("Vous avez joué en colonne "+ numCol + "\n")
                            validInput = true
                            fin = jeu.joue(numCol, numJoueur)          // On effectue le tour 
                            //Thread.sleep(2000)
                        }
                        else { println("Colonne saturée, veuillez choisir une autre colonne\n")} //Cas de colonne invalide
                        } 
                }
            }
        }
        //Résultat du jeu et affichage
        jeu.afficherJeu()
        println("//=================================================//")
        println("\n\tLe joueur " + numJoueur + " a gagné !!\n")
        println("//=================================================//")

        
    }

    // -------------------------------------------------------------------------//
    // Fait jouer l'IA à partir d'un plateau donné
    // L'IA est toujours le joueur 2, représenté par un "O"
    // Le joueur est le joueur 1, représenté par un "X"
    // -------------------------------------------------------------------------//
    def joueCoupOrdi(plateau: Jeu): Jeu ={
         val ia = 2
         val joueur = 1
         var ai = new IA(plateau, ia, joueur)        
        var col = ai.choisirMeilleurCoup(plateau, ia)   
        println("L'IA a joué en colonne "+ col + "\n")
        val fin = plateau.joue(col, ia)
        plateau.afficherJeu()
        if (fin) println("\nL'IA a gagné\n")
        plateau
    }

    // -------------------------------------------------------------------------//
    // Fait jouer le joueur à partir d'un plateau donné
    // L'IA est toujours le joueur 2, représenté par un "O"
    // Le joueur est le joueur 1, représenté par un "X"
    // -------------------------------------------------------------------------//
    def joueCoupHumain(plateau: Jeu, col: Int): Jeu = {
        if (plateau.coupValide(col)){
            val fin = plateau.joue(col, 1)
            if (fin) {println("\nVous avez gagné\n")}
            (plateau)
        }
        else { 
            println("\nCoup invalide\n")
            plateau
        }
        
    }

    


    // -------------------------------------------------------------------------//
    // ------------------- Partie à 1 joueur contre une IA ---------------------//
    // -------------------------------------------------------------------------//
    def partieIA() : Unit = {
        println("Nouvelle partie\n")
        val jeu = new Jeu(m, n)
        var fin : Boolean = false                   //Booléen qui décrit qu'un joueur a gagné, ou que les 2 joueurs n'ont plus de jetons
        var numJoueur = 1+Random.nextInt(2)
        var compteur = 0                            //Compteur qui gère qu'on ne dépasse pas les m * n tours (=nombre de jetons = taille de la grille)
        while(!fin && compteur < m * n){
            numJoueur = 1 + numJoueur%2             //Alterne entre joueur 1 et joueur 2
            jeu.afficherJeu()
            var validInput = false
            println("//-------------- "+ {if (numJoueur==1) "C'est votre tour: " else "IA"} +" --------------//")
            // -------------------------------------------------------------------------//
            // Boucle de vérification de l'input
            // -------------------------------------------------------------------------//
            if (numJoueur == 1){
                while (!validInput)
            {
                println("Saisir numéro de colonne:  ")
                var input = readLine()
                println("\n")
                tryToInt(input) match
                {
                    
                    case Some(-1)     => System.exit(0) //Permet à l'utilisateur de quitter la partie
                    case None => {                      //Cas d'erreur lorsque l'utilisateur saisit une chaîne de caractères par exemple
                        jeu.afficherJeu()
                        println("/!\\ Erreur, veuillez saisir une valeur comprise entre 1 et " + n + "\nPour quitter, tapez -1\n")
                    }

                    case Some(p) if (p.toInt < 1 || p.toInt > n) => {                      //Cas d'erreur lorsque l'utilisateur saisit une chaîne de caractères par exemple
                        jeu.afficherJeu()
                        println("/!\\ Erreur, veuillez saisir une valeur comprise entre 1 et " + n + "\nPour quitter, tapez -1\n")
                    }

                    case Some(p) if (p.toInt >= 1 && p.toInt <= n)  => // Cas normal, l'utilisateur saisit un entier compris entre 1 et n
                        {
                        val numCol = p.toInt
                        if (jeu.coupValide(numCol) )                   // On vérifie que la colonne choisie est valide
                        {
                            println("Vous avez joué en colonne "+ numCol + "\n")
                            validInput = true
                            fin = jeu.joue(numCol, numJoueur)          // On effectue le tour 
                            //Thread.sleep(2000)
                        }
                        else { println("Colonne saturée, veuillez choisir une autre colonne\n")} //Cas de colonne invalide
                        }
                }
            }
        }
            
            //Partie IA
            else 
            { 
                
                var ai = new IA(jeu, numJoueur, numJoueur-1)        //On instancie une IA en fonction du jeu
                var col = ai.choisirMeilleurCoup(jeu, numJoueur)    // L'IA choisit la meilleure colonne 
                println("L'IA a joué en colonne "+ col + "\n")
                fin = jeu.joue(col, numJoueur)                      // On joue et vérifie si l'IA a gagné
            }

            compteur += 1 

        }
        //Résultat du jeu et affichage
        jeu.afficherJeu()
        println("//=================================================//")
        println("\n\tLe joueur " + numJoueur + " a gagné !!\n")
        println("//=================================================//")

    }

    partieIA()
    //partie()

    }
